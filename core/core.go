package core

import "fmt"

const height int = 25
const width int = 25

type Core struct {
	data [height][width]int8
}

func (c *Core) Clear() {
	for i := 0; i < height; i++ {
		for j := 0; j < width; j++ {
			c.data[i][j] = 0
		}
	}
}

func (c *Core) Seed() {
	c.data[11][12] = 1
	c.data[12][13] = 1
	c.data[13][11] = 1
	c.data[13][12] = 1
	c.data[13][13] = 1
}

func (c *Core) Iterate() {
	var liveCount int8
	var isAlive bool
	var willBeAlive bool

	for i := 0; i < height; i++ {
		for j := 0; j < width; j++ {
			isAlive = c.data[i][j]%2 == 1
			liveCount = c.liveCount(i, j)

			willBeAlive = false
			if isAlive {
				if liveCount == 2 || liveCount == 3 {
					willBeAlive = true
				}
			} else {
				if liveCount == 3 {
					willBeAlive = true
				}
			}

			switch {
			case !willBeAlive && !isAlive:
				c.data[i][j] = 2
			case !willBeAlive && isAlive:
				c.data[i][j] = 3
			case willBeAlive && !isAlive:
				c.data[i][j] = 4
			case willBeAlive && isAlive:
				c.data[i][j] = 5
			}
		}
	}

	for i := 0; i < height; i++ {
		for j := 0; j < width; j++ {
			c.data[i][j] = (c.data[i][j] / 2) - 1
		}
	}
}

func (c *Core) Show() {
	line := make([]rune, width+2)

	line[0] = '╔'
	line[width+1] = '╗'
	for j := 1; j < width+1; j++ {
		line[j] = '═'
	}
	fmt.Println(string(line))

	line[0] = '║'
	line[width+1] = '║'
	for i := 0; i < height; i++ {
		for j := 0; j < width; j++ {
			if c.data[i][j] == 1 {
				line[j+1] = '*'
			} else {
				line[j+1] = ' '
			}
		}
		fmt.Println(string(line))
	}

	line[0] = '╚'
	line[width+1] = '╝'
	for j := 1; j < width+1; j++ {
		line[j] = '═'
	}
	fmt.Println(string(line))
}

func (c *Core) liveCount(row, col int) (res int8) {
	if col > 0 && c.data[row][col-1]%2 == 1 {
		res = res + 1
	}
	if col < width-1 && c.data[row][col+1]%2 == 1 {
		res = res + 1
	}
	if row > 0 {
		if c.data[row-1][col]%2 == 1 {
			res = res + 1
		}
		if col > 0 && c.data[row-1][col-1]%2 == 1 {
			res = res + 1
		}
		if col < width-1 && c.data[row-1][col+1]%2 == 1 {
			res = res + 1
		}
	}
	if row < height-1 {
		if c.data[row+1][col]%2 == 1 {
			res = res + 1
		}
		if col > 0 && c.data[row+1][col-1]%2 == 1 {
			res = res + 1
		}
		if col < width-1 && c.data[row+1][col+1]%2 == 1 {
			res = res + 1
		}
	}
	return
}
