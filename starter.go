package main

import (
	"gameOfLife/core"
	"time"
)

func main() {
	var c core.Core

	c.Seed()
	c.Show()

	for true {
		time.Sleep(time.Second)
		c.Iterate()
		c.Show()
	}
}
